#pragma once

#include <esp_log.h>

#include <chrono>
#include <hedley.h>

#ifdef __clang__
#define PMR_DIAGNOSTIC_IGNORE_CONVERSION HEDLEY_PRAGMA(clang diagnostic ignored "-Wconversion")
#elif __GNUG__
#define PMR_DIAGNOSTIC_IGNORE_CONVERSION HEDLEY_PRAGMA(GCC diagnostic ignored "-Wconversion")
#else
#define PMR_DIAGNOSTIC_IGNORE_CONVERSION
#endif

#define PMR_ERROR_CHECK(x)                                                           \
    do                                                                               \
    {                                                                                \
        esp_err_t err_rc_ = (x);                                                     \
        if (unlikely(err_rc_ != ESP_OK))                                             \
        {                                                                            \
            ESP_LOGE(PMR_TAG, "Call to " #x " failed in %s:%d", __FILE__, __LINE__); \
            abort();                                                                 \
        }                                                                            \
    } while (0)

using PmrTimePoint = std::chrono::time_point<std::chrono::system_clock>;

inline static constexpr auto PMR_TAG = "PMR";
