#pragma once

#include "pmr.hh"

#include <cstdint>
#include <string_view>

struct OtaConfig
{
    /// URI of the OTA image (i.e. https://domain/ota_image_name.bin)
    std::string_view url;
    /// The certificate used by the server.
    /// The certificate date must be in text form with a '\0' at the end.
    const std::uint8_t *ota_server_certificate;
};

enum class OtaError
{
    /// No error
    Ok,
    /// A connection to the OTA server has not been established
    NoConnection,
    /// Invalid configuration parameters
    InvalidArgument,
    /// Failed to read update image descriptor
    ImageInfoReadFailed,
    /// Invalid application update image
    ValidateFailed,
    /// Failed to allocate memory
    AllocationFailed,
    /// Flash write failed
    FlashError,
    /// Unknown error
    Unknown,
};

OtaError ota_establish_connection(OtaConfig &&config);
OtaError ota_close_connection();

OtaError ota_is_update_available(bool &result);
OtaError ota_execute_update();

OtaError ota_remote_compilation_date(PmrTimePoint &result);

bool ota_try_execute_update(const char *url, const std::uint8_t *certificate);