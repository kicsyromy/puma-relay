#pragma once

#include "pmr.hh"

#include <esp_err.h>

#include <string>

esp_err_t nvs_open_namespace(const char *id);
void      nvs_close_namespace();
void      nvs_flush();

esp_err_t nvs_blob_size(const char *key, std::size_t &result);
esp_err_t nvs_read_blob(const char *key, void *result, std::size_t size);
esp_err_t nvs_write_blob(const char *key, const void *value, std::size_t size);

template<typename T> inline esp_err_t nvs_read(const char *key, T &result)
{
    return nvs_read_blob(key, &result, sizeof(T));
}

template<typename T> inline esp_err_t nvs_write(const char *key, const T &value)
{
    return nvs_write_blob(key, &value, sizeof(T));
}

template<> inline esp_err_t nvs_read<std::string>(const char *key, std::string &result)
{
    auto len = std::size_t{};
    nvs_blob_size(key, len);

    result.resize(len, '\0');
    return nvs_read_blob(key, result.data(), len);
}

template<>
inline esp_err_t nvs_write<std::string_view>(const char *key, const std::string_view &value)
{
    return nvs_write_blob(key, value.data(), value.size());
}

template<> inline esp_err_t nvs_write<std::string>(const char *key, const std::string &value)
{
    return nvs_write_blob(key, value.data(), value.size());
}
