#include "nvs.hh"
#include "ota.hh"
#include "pmr.hh"
#include "resources.h"
#include "wifi.hh"

// FIXME: We get a conversion error caused by fmt::format so disable the warning in this translation
//        unit
HEDLEY_DIAGNOSTIC_PUSH
PMR_DIAGNOSTIC_IGNORE_CONVERSION
#include <fmt/format.h>
HEDLEY_DIAGNOSTIC_POP

#include <sys/param.h>

#include <driver/gpio.h>
#include <driver/uart.h>
#include <esp_log.h>
#include <esp_mac.h>
#include <esp_netif.h>
#include <esp_system.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>

#include <lwip/dns.h>
#include <lwip/err.h>
#include <lwip/sockets.h>

#include <array>
#include <cstring>
#include <string>
#include <vector>

#if CONFIG_PMR_UART_PARITY_DISABLE
#define CONFIG_PMR_UART_PARITY UART_PARITY_DISABLE
#elif CONFIG_PMR_UART_PARITY_EVEN
#define CONFIG_PMR_UART_PARITY UART_PARITY_EVEN
#elif CONFIG_PMR_UART_PARITY_ODD
#define CONFIG_PMR_UART_PARITY UART_PARITY_ODD
#endif

#if CONFIG_PMR_UART_STOP_BITS_1
#define CONFIG_PMR_UART_STOP_BITS UART_STOP_BITS_1
#elif CONFIG_PMR_UART_STOP_BITS_1_5
#define CONFIG_PMR_UART_STOP_BITS UART_STOP_BITS_1_5
#elif CONFIG_PMR_UART_STOP_BITS_2
#define CONFIG_PMR_UART_STOP_BITS UART_STOP_BITS_2
#endif

enum Register : std::uint8_t
{
    Power = 0,

    OutChannelA   = 1,
    OutChannelB   = 2,
    OutChannelLed = 3,

    UartPort    = 4,
    BaudRate    = 5,
    DataBits    = 6,
    StopBits    = 7,
    Parity      = 8,
    RxThreshold = 9,

    ServerFqdn = 10,
    ServerPort = 11,

    WiFiAccessPoint    = 12,
    WiFiPassword       = 13,
    WiFiAuthentication = 14,

    OtaUpdate = 15,

    DeviceId = 16,

    Count
};

struct ModbusRegisters
{
    int                BaudRate{ CONFIG_PMR_UART_BAUD_RATE };
    uart_word_length_t DataBits{ static_cast<uart_word_length_t>(CONFIG_PMR_UART_DATA_BITS - 5) };
    uart_stop_bits_t   StopBits{ CONFIG_PMR_UART_STOP_BITS };
    uart_parity_t      Parity{ CONFIG_PMR_UART_PARITY };
    std::uint8_t       RxThreshold{ CONFIG_PMR_UART_RX_THRESHOLD };

    std::string   ServerFqdn{ CONFIG_PMR_SERVER_FQDN };
    std::uint16_t ServerPort{ CONFIG_PMR_SERVER_PORT };

    std::string      WiFiAccessPoint{ CONFIG_PMR_WIFI_DEFAULT_AP };
    std::string      WiFiPassword{ CONFIG_PMR_WIFI_DEFAULT_PASSWORD };
    wifi_auth_mode_t WiFiAuthentication{ CONFIG_PMR_WIFI_DEFAULT_AUTH_MODE };
} MODBUS_REGISTERS;

#define PMR_NVS_READ_REGISTER(name)                      \
    do                                                   \
    {                                                    \
        auto result = decltype(ModbusRegisters::name){}; \
        if (nvs_read(#name, result) == ESP_OK)           \
        {                                                \
            MODBUS_REGISTERS.name = result;              \
        }                                                \
    } while (0)

#define PMR_NVS_WRITE_REGISTER(name, value)                                               \
    do                                                                                    \
    {                                                                                     \
        static_assert(std::is_same_v<std::remove_cv_t<decltype(value)>,                   \
                                     std::remove_cv_t<decltype(MODBUS_REGISTERS.name)>>); \
        if (nvs_write(#name, value) != ESP_OK)                                            \
        {                                                                                 \
            ESP_LOGE(PMR_TAG, "Failed to write " #name " to nvs");                        \
        }                                                                                 \
        else                                                                              \
        {                                                                                 \
            nvs_flush();                                                                  \
        }                                                                                 \
    } while (0)

#define RS485 CONFIG_PMR_UART_PORT

#define BLINK_GPIO static_cast<gpio_num_t>(CONFIG_PMR_GPIO_OUT_CHANNEL_LED)

#define OUT_CH_A static_cast<gpio_num_t>(CONFIG_PMR_GPIO_OUT_CHANNEL_A)
#define OUT_CH_B static_cast<gpio_num_t>(CONFIG_PMR_GPIO_OUT_CHANNEL_B)

// Pins
#define RS485_DE GPIO_NUM_2
#define RS485_RE GPIO_NUM_4
#define RS485_TX 27
#define RS485_RX 26

// Sizes
#define RS485_RX_BUFF_SIZE (UART_FIFO_LEN + 32)
#define RS485_TX_BUFF_SIZE 0

// Times
#define RS485_RX_TO 0

static constexpr std::uint8_t RS485_OK_NVS_WRITE          = 0;
static constexpr std::uint8_t RS485_OK_OTA                = 132;
static constexpr std::uint8_t RS485_ERR_WRITE             = 1;
static constexpr std::uint8_t RS485_ERR_READ_INITIAL      = 2;
static constexpr std::uint8_t RS485_ERR_READ_REST         = 3;
static constexpr std::uint8_t RS485_ERR_NOT_IMPL          = 128;
static constexpr std::uint8_t RS485_ERR_NVS_INVALID_VALUE = 129;
static constexpr std::uint8_t RS485_ERR_CRC               = 130;
static constexpr std::uint8_t RS485_ERR_OTA_FAIL          = 131;

// Magic =_=
static const uint8_t table_crc_hi[] = {
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
    0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
};

static const uint8_t table_crc_lo[] = {
    0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04,
    0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
    0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
    0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10,
    0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
    0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
    0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C,
    0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0,
    0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
    0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
    0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C,
    0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
    0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54,
    0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98,
    0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
    0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40
};

// More magic x_X
static uint16_t crc16(uint8_t      *buffer,
                      uint16_t      buffer_length,
                      std::uint8_t *hi = nullptr,
                      std::uint8_t *lo = nullptr)
{
    uint8_t      crc_hi = 0xFF; /* high CRC byte initialized */
    uint8_t      crc_lo = 0xFF; /* low CRC byte initialized */
    unsigned int i;             /* will index into CRC lookup */

    /* pass through message buffer */
    while (buffer_length--)
    {
        i      = crc_hi ^ *buffer++; /* calculate the CRC  */
        crc_hi = crc_lo ^ table_crc_hi[i];
        crc_lo = table_crc_lo[i];
    }

    if (hi)
    {
        *hi = crc_hi;
    }

    if (lo)
    {
        *lo = crc_lo;
    }

    return (static_cast<uint16_t>(crc_hi << 8 | crc_lo));
}

static void configure_nvs()
{
    nvs_open_namespace("PumaModbusRelay");

    PMR_NVS_READ_REGISTER(BaudRate);
    PMR_NVS_READ_REGISTER(DataBits);
    PMR_NVS_READ_REGISTER(StopBits);
    PMR_NVS_READ_REGISTER(Parity);
    PMR_NVS_READ_REGISTER(RxThreshold);

    PMR_NVS_READ_REGISTER(ServerFqdn);
    PMR_NVS_READ_REGISTER(ServerPort);

    PMR_NVS_READ_REGISTER(WiFiAccessPoint);
    PMR_NVS_READ_REGISTER(WiFiPassword);
    PMR_NVS_READ_REGISTER(WiFiAuthentication);
}

static void configure_led()
{
    gpio_reset_pin(BLINK_GPIO);
    gpio_set_direction(BLINK_GPIO, GPIO_MODE_INPUT_OUTPUT);
}

static void configure_outputs()
{
    gpio_reset_pin(OUT_CH_A);
    gpio_reset_pin(OUT_CH_B);
    gpio_set_direction(OUT_CH_A, GPIO_MODE_INPUT_OUTPUT);
    gpio_set_direction(OUT_CH_B, GPIO_MODE_INPUT_OUTPUT);

    gpio_set_level(OUT_CH_A, 0);
    gpio_set_level(OUT_CH_B, 0);
}

static void configure_rs485_control_lines()
{
    gpio_reset_pin(RS485_RE);
    gpio_set_direction(RS485_RE, GPIO_MODE_OUTPUT);
}

static void configure_rs485()
{
    configure_rs485_control_lines();
    auto uart_config                = uart_config_t{};
    uart_config.source_clk          = uart_sclk_t::UART_SCLK_DEFAULT;
    uart_config.baud_rate           = MODBUS_REGISTERS.BaudRate;
    uart_config.data_bits           = MODBUS_REGISTERS.DataBits;
    uart_config.parity              = MODBUS_REGISTERS.Parity;
    uart_config.stop_bits           = MODBUS_REGISTERS.StopBits;
    uart_config.flow_ctrl           = UART_HW_FLOWCTRL_DISABLE;
    uart_config.rx_flow_ctrl_thresh = MODBUS_REGISTERS.RxThreshold; // WTF is this?

    PMR_ERROR_CHECK(
        uart_driver_install(RS485, RS485_RX_BUFF_SIZE, RS485_TX_BUFF_SIZE, 0, nullptr, 0));
    PMR_ERROR_CHECK(uart_param_config(RS485, &uart_config));
    PMR_ERROR_CHECK(uart_set_pin(RS485, RS485_TX, RS485_RX, RS485_DE, -1));
    PMR_ERROR_CHECK(uart_set_mode(RS485, UART_MODE_RS485_HALF_DUPLEX));
    PMR_ERROR_CHECK(uart_set_rx_timeout(RS485, RS485_RX_TO));
}

static void configure_wifi()
{
    wifi_configure(MODBUS_REGISTERS.WiFiAccessPoint.data(),
                   MODBUS_REGISTERS.WiFiPassword.data(),
                   MODBUS_REGISTERS.WiFiAuthentication);
    wifi_start();
}

/**
 * Construct an error response in case the device was not able to read/write data from the RS485
 * bus.
 */
static std::vector<uint8_t> construct_response(const std::uint8_t *data, std::uint8_t len)
{
    ESP_LOGE(PMR_TAG, "Constructing response");
    auto buffer = std::vector<uint8_t>{};
    buffer.resize(static_cast<std::size_t>(len + 5));

    buffer[0] = 0;
    buffer[1] = 3;
    buffer[2] = len;
    for (std::size_t i = 0; i < len; ++i)
    {
        buffer[i + 3] = static_cast<std::uint8_t>(data[i]);
    }
    uint16_t crc              = crc16(buffer.data(), static_cast<uint16_t>(buffer.size() - 2));
    buffer[buffer.size() - 2] = static_cast<std::uint8_t>(crc >> 8);
    buffer[buffer.size() - 1] = static_cast<std::uint8_t>(crc & 0x00ff);

    ESP_LOGE(PMR_TAG, "Returning payload.");
    return buffer;
}

/**
 * Perform a transaction on the configured RS485 UART.
 * This function will look into the response payload and determine how many bytes it needs to read.
 */
static std::vector<uint8_t> modbus_transact(uint8_t *buffer, int buffer_len)
{
    uart_set_rx_full_threshold(RS485, 1);
    auto response_buffer = std::vector<std::uint8_t>{};
    response_buffer.resize(8);
    memset(response_buffer.data(), 0, 8);
    ESP_LOGW(PMR_TAG, "Writing %d on bus.", buffer_len);
    ESP_LOGW(PMR_TAG,
             "%x %x %x %x %x %x %x %x",
             buffer[0],
             buffer[1],
             buffer[2],
             buffer[3],
             buffer[4],
             buffer[5],
             buffer[6],
             buffer[7]);
    if (uart_write_bytes(RS485, buffer, static_cast<size_t>(buffer_len)) != buffer_len)
    {
        ESP_LOGE(PMR_TAG, "Failed to write on bus.");
        ESP_LOGE(PMR_TAG, "Returning fault response");
        return construct_response(&RS485_ERR_WRITE, 1);
    }
    int read_on_response = 0;
    // TickType_t char_time_ms = 1000 * (2 + 8 + 1) / 38400;
    int total_read = read_on_response;
    do
    {
        response_buffer.resize(static_cast<std::size_t>(total_read + 8));
        memset(response_buffer.data() + total_read, 0, 8);
        read_on_response = uart_read_bytes(RS485, response_buffer.data() + total_read, 8, 4);
        total_read += read_on_response;
    } while (read_on_response > 0);
    response_buffer.resize(static_cast<std::size_t>(total_read));
    ESP_LOGW(PMR_TAG,
             "Got a response of %d bytes. CRC %d %d",
             total_read,
             response_buffer[static_cast<std::size_t>(total_read - 2)],
             response_buffer[static_cast<std::size_t>(total_read - 1)]);
    return response_buffer;
}

static void blink_led()
{
    static uint8_t s_led_state = 0;

    gpio_set_level(BLINK_GPIO, s_led_state);
    s_led_state = !s_led_state;
}

static std::vector<uint8_t> handle_modbus_packet(uint8_t *request, int request_len)
{
    blink_led();
    if (request[0] == 0)
    {
        ESP_LOGW(PMR_TAG, "Request is for PUMA");

        std::uint16_t reg{};
        reg |= static_cast<std::uint16_t>(request[2] << 8);
        reg = static_cast<std::uint16_t>(reg | request[3]);

        std::uint16_t request_data_len{};
        request_data_len |= static_cast<std::uint16_t>(request[4] << 8);
        request_data_len = static_cast<std::uint16_t>(request_data_len | request[5]);

        std::uint8_t comp_hi, comp_lo;
        const auto   computed_crc =
            crc16(request, static_cast<uint16_t>(request_len - 2), &comp_hi, &comp_lo);
        auto sent_crc = std::uint16_t{ 0 };
        sent_crc |= static_cast<std::uint16_t>(request[request_len - 2] << 8);
        sent_crc = static_cast<std::uint16_t>(sent_crc | request[request_len - 1]);
        if (computed_crc != sent_crc)
        {
            std::string payload_str{};
            payload_str.resize(static_cast<std::size_t>(request_len * 2));
            for (int i = 0; i < request_len; ++i)
            {
                fmt::format_to(payload_str.begin() + i * 2, "{:02x}", request[i]);
            }
            ESP_LOGE(PMR_TAG, "Request: %s", payload_str.c_str());
            ESP_LOGE(PMR_TAG, "CRC Computed: HI: %d, LO: %d", comp_hi, comp_lo);
            ESP_LOGE(PMR_TAG,
                     "CRC Received: HI: %d, LO: %d",
                     request[request_len - 2],
                     request[request_len - 1]);
            ESP_LOGE(PMR_TAG,
                     "Failed to validate modbus frame: %d (computed) != %d (received)",
                     computed_crc,
                     sent_crc);
            return construct_response(&RS485_ERR_CRC, 1);
        }

        switch (reg)
        {
        case Power: {
            if (request_data_len == 1)
            {
                nvs_close_namespace();
                esp_restart();
            }
            else
            {
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
        }

        case DeviceId: {
            auto mac_address = std::array<std::uint8_t, 6>{};
            PMR_ERROR_CHECK(esp_efuse_mac_get_default(mac_address.data()));
            ESP_LOGI(PMR_TAG,
                     "Responding with %x %x %x %x %x %x",
                     mac_address[0],
                     mac_address[1],
                     mac_address[2],
                     mac_address[3],
                     mac_address[4],
                     mac_address[5]);
            return construct_response(mac_address.data(), mac_address.size());
        }

        case OutChannelA: {
            if (request_data_len == 1)
            {
                const auto value = request[6];
                ESP_LOGI(PMR_TAG, "Setting channel A to %d", value);
                gpio_set_level(OUT_CH_A, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                const auto response = static_cast<uint8_t>(gpio_get_level(OUT_CH_A));
                return construct_response(&response, 1);
            }
        }
        break;

        case OutChannelB: {
            if (request_data_len == 1)
            {
                const auto value = request[6];
                gpio_set_level(OUT_CH_B, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                const auto response = static_cast<uint8_t>(gpio_get_level(OUT_CH_B));
                return construct_response(&response, 1);
            }
        }
        break;

        case OutChannelLed: {
            if (request_data_len == 1)
            {
                const auto value = request[6];
                gpio_set_level(BLINK_GPIO, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                const auto response = static_cast<uint8_t>(gpio_get_level(BLINK_GPIO));
                return construct_response(&response, 1);
            }
        }
        break;

        case BaudRate: {
            if (request_data_len == 4)
            {
                auto value = int{};
                value |= request[6] << 24;
                value |= request[7] << 16;
                value |= request[8] << 8;
                value |= request[9] << 0;
                PMR_NVS_WRITE_REGISTER(BaudRate, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                auto response = std::array<std::uint8_t, 4>{};
                response[0]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.BaudRate >> 24);
                response[1]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.BaudRate >> 16);
                response[2]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.BaudRate >> 8);
                response[3]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.BaudRate >> 0);
                return construct_response(response.data(), response.size());
            }
        }
        break;

        case DataBits: {
            if (request_data_len == 1)
            {
                auto value = request[6];
                switch (value)
                {
                case 5:
                case 6:
                case 7:
                case 8:
                    PMR_NVS_WRITE_REGISTER(DataBits, static_cast<uart_word_length_t>(value - 5));
                    return construct_response(&RS485_OK_NVS_WRITE, 1);

                default:
                    ESP_LOGE(PMR_TAG,
                             "Invalid value received for DataBits, expected one of [5, 8], got %d. "
                             "Ignored.",
                             value);
                    return construct_response(&RS485_ERR_NVS_INVALID_VALUE, 1);
                }
            }
            else
            {
                const auto response = static_cast<uint8_t>(MODBUS_REGISTERS.DataBits + 5);
                return construct_response(&response, 1);
            }
        }
        break;

        case StopBits: {
            if (request_data_len == 1)
            {
                auto value = request[6];
                switch (value)
                {
                case 1:
                case 2:
                case 3:
                    PMR_NVS_WRITE_REGISTER(StopBits, static_cast<uart_stop_bits_t>(value));
                    return construct_response(&RS485_OK_NVS_WRITE, 1);

                default:
                    ESP_LOGE(PMR_TAG,
                             "Invalid value received for StopBits, expected one of [1, 3], got %d. "
                             "Ignored.",
                             value);
                    return construct_response(&RS485_ERR_NVS_INVALID_VALUE, 1);
                }
            }
            else
            {
                const auto response = static_cast<uint8_t>(MODBUS_REGISTERS.StopBits);
                return construct_response(&response, 1);
            }
        }
        break;

        case Parity: {
            if (request_data_len == 1)
            {
                auto value = request[6];
                switch (value)
                {
                case 0:
                case 2:
                case 3:
                    PMR_NVS_WRITE_REGISTER(Parity, static_cast<uart_parity_t>(value));
                    return construct_response(&RS485_OK_NVS_WRITE, 1);

                default:
                    ESP_LOGE(
                        PMR_TAG,
                        "Invalid value received for StopBits, expected one of 0, 2, 3, got %d. "
                        "Ignored.",
                        value);
                    return construct_response(&RS485_ERR_NVS_INVALID_VALUE, 1);
                }
            }
            else
            {
                const auto response = static_cast<uint8_t>(MODBUS_REGISTERS.Parity);
                return construct_response(&response, 1);
            }
        }
        break;

        case RxThreshold: {
            if (request_data_len == 1)
            {
                const auto value = request[6];
                PMR_NVS_WRITE_REGISTER(RxThreshold, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                const auto response = MODBUS_REGISTERS.RxThreshold;
                return construct_response(&response, 1);
            }
        }
        break;

        case ServerFqdn: {
            if (request_data_len == 0)
            {
                return construct_response(
                    reinterpret_cast<const std::uint8_t *>(MODBUS_REGISTERS.ServerFqdn.c_str()),
                    static_cast<uint8_t>(MODBUS_REGISTERS.ServerFqdn.size()));
            }

            const auto data = std::string{ reinterpret_cast<const char *>(&request[6]),
                                           static_cast<std::size_t>(request_data_len) };
            PMR_NVS_WRITE_REGISTER(ServerFqdn, data);
            return construct_response(&RS485_OK_NVS_WRITE, 1);
        }
        break;

        case ServerPort: {
            if (request_data_len == 2)
            {
                std::uint16_t value{};
                request_data_len |= static_cast<std::uint8_t>(request[6] << 8);
                request_data_len = static_cast<std::uint16_t>(request_data_len | request[8]);
                PMR_NVS_WRITE_REGISTER(ServerPort, value);
                return construct_response(&RS485_OK_NVS_WRITE, 1);
            }
            else
            {
                auto response = std::array<std::uint8_t, 2>{};
                response[0]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.ServerPort >> 8);
                response[1]   = static_cast<std::uint8_t>(MODBUS_REGISTERS.ServerPort >> 0);
                return construct_response(response.data(), response.size());
            }
        }
        break;

        case WiFiAccessPoint: {
            if (request_data_len == 0)
            {
                return construct_response(
                    reinterpret_cast<const std::uint8_t *>(
                        MODBUS_REGISTERS.WiFiAccessPoint.c_str()),
                    static_cast<uint8_t>(MODBUS_REGISTERS.WiFiAccessPoint.size()));
            }

            const auto data = std::string{ reinterpret_cast<const char *>(&request[6]),
                                           static_cast<std::size_t>(request_data_len) };
            PMR_NVS_WRITE_REGISTER(WiFiAccessPoint, data);
            return construct_response(&RS485_OK_NVS_WRITE, 1);
        }
        break;

        case WiFiPassword: {
            if (request_data_len == 0)
            {
                return construct_response(
                    reinterpret_cast<const std::uint8_t *>(MODBUS_REGISTERS.WiFiPassword.c_str()),
                    static_cast<uint8_t>(MODBUS_REGISTERS.WiFiPassword.size()));
            }

            const auto data = std::string{ reinterpret_cast<const char *>(&request[6]),
                                           static_cast<std::size_t>(request_data_len) };
            PMR_NVS_WRITE_REGISTER(WiFiPassword, data);
            return construct_response(&RS485_OK_NVS_WRITE, 1);
        }
        break;

        case OtaUpdate: {
            if (ota_try_execute_update("https://puma-ota.kicsyromy.me/latest.bin",
                                       resource_ota_electrovic_ro))
            {
                return construct_response(&RS485_OK_OTA, 1);
            }
            else
            {
                return construct_response(&RS485_ERR_OTA_FAIL, 1);
            }
        }
        break;

        case UartPort:
        case WiFiAuthentication:
        case Count:
        default:
            return construct_response(&RS485_ERR_NOT_IMPL, 1);
            break;
        }
    }
    else
    {
        ESP_LOGW(PMR_TAG, "Request is for slave %d", request[0]);
        return modbus_transact(request, request_len);
    }
}

static void handle_and_respond_to_modbus_messages(int sock)
{
    int     len;
    uint8_t rx_buffer[128];

    do
    {
        ESP_LOGW(PMR_TAG, "Waiting for request.");
        len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
        if (len < 0)
        {
            const auto err = errno;
            // If a timeout occurs on the socket it means the server hasn't sent any data
            if (err == EAGAIN)
            {
                // Try again, the call to recv() should fail if the server disconnects
                // in the meantime
                continue;
            }

            ESP_LOGE(PMR_TAG, "Error occurred during receiving: errno %d", err);
        }
        else if (len == 0)
        {
            ESP_LOGW(PMR_TAG, "Connection closed");
        }
        else
        {
            ESP_LOGW(PMR_TAG, "Request received.");
            auto tx_buffer = handle_modbus_packet(rx_buffer, len);
            // send() can return fewer bytes than supplied length.
            // Walk-around for robust implementation.
            ESP_LOGE(PMR_TAG, "Got response, sending");
            auto to_write = static_cast<int>(tx_buffer.size());
            do
            {
                const auto written =
                    send(sock,
                         tx_buffer.data() + (tx_buffer.size() - static_cast<std::size_t>(to_write)),
                         static_cast<std::size_t>(to_write),
                         0);
                if (written < 0)
                {
                    ESP_LOGE(PMR_TAG, "Error occurred during sending: errno %d", errno);
                    return;
                }
                to_write -= written;
            } while (to_write > 0);
            ESP_LOGE(PMR_TAG, "Response sent");
            ESP_LOGE(PMR_TAG, "Buffer freed");
        }
    } while (len > 0);
}

static void tcp_client_task(void *)
{
    static constexpr auto DNS_RESOLVE_SUCCESS_BIT = BIT0;
    static constexpr auto DNS_RESOLVE_FAIL_BIT    = BIT1;

    auto dns_resolved_event_gr = xEventGroupCreate();
    auto ip_address            = ip_addr_t{};

    struct CallbackData
    {
        EventGroupHandle_t &event_group;
        ip_addr_t          &ip;
    } callback_data{ dns_resolved_event_gr, ip_address };
    dns_gethostbyname_addrtype(
        MODBUS_REGISTERS.ServerFqdn.c_str(),
        &ip_address,
        [](const char *name, const ip_addr_t *ipaddr, void *callback_arg) {
            auto &cb_data = *static_cast<CallbackData *>(callback_arg);
            if (ipaddr != nullptr)
            {
                cb_data.ip = *ipaddr;
                xEventGroupSetBits(cb_data.event_group, DNS_RESOLVE_SUCCESS_BIT);
            }
            else
            {
                ESP_LOGE(PMR_TAG, "Failed to resolve hostname: %s", name);
                xEventGroupSetBits(cb_data.event_group, DNS_RESOLVE_FAIL_BIT);
            }
        },
        &callback_data,
        LWIP_DNS_ADDRTYPE_IPV4_IPV6);

    const auto bits = xEventGroupWaitBits(dns_resolved_event_gr,
                                          DNS_RESOLVE_SUCCESS_BIT | DNS_RESOLVE_FAIL_BIT,
                                          pdFALSE,
                                          pdFALSE,
                                          portMAX_DELAY);
    if (bits & DNS_RESOLVE_FAIL_BIT)
    {
        esp_restart();
    }

    vEventGroupDelete(dns_resolved_event_gr);

    std::string ip_address_str;
    if (ip_address.type == IPADDR_TYPE_V4)
    {
        ip_address_str = ip4addr_ntoa(&ip_address.u_addr.ip4);
    }
    else
    {
        ip_address_str = ip6addr_ntoa(&ip_address.u_addr.ip6);
    }

    auto socket_timeout    = timeval{};
    socket_timeout.tv_sec  = 15;
    socket_timeout.tv_usec = 0;

    for (;;)
    {
        int err  = 0;
        int sock = 0;
        if (ip_address.type == IPADDR_TYPE_V4)
        {
            auto dest_addr = sockaddr_in{};
            inet_pton(AF_INET, ip_address_str.c_str(), &dest_addr.sin_addr.s_addr);
            dest_addr.sin_family   = AF_INET;
            dest_addr.sin_port     = htons(MODBUS_REGISTERS.ServerPort);
            const auto addr_family = AF_INET;
            const auto ip_protocol = IPPROTO_IP;

            sock = socket(addr_family, SOCK_STREAM, ip_protocol);
            if (sock < 0)
            {
                ESP_LOGE(PMR_TAG, "Unable to create socket: errno %d", errno);
                break;
            }

            auto sock_opt_result =
                setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &socket_timeout, sizeof(socket_timeout));
            if (sock_opt_result < 0)
            {
                ESP_LOGE(PMR_TAG,
                         "Failed to set socket timeout (RECV): %s",
                         std::strerror(sock_opt_result));
                esp_restart();
            }

            sock_opt_result =
                setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &socket_timeout, sizeof(socket_timeout));
            if (sock_opt_result < 0)
            {
                ESP_LOGE(PMR_TAG,
                         "Failed to set socket timeout (SEND): %s",
                         std::strerror(sock_opt_result));
                esp_restart();
            }

            ESP_LOGI(PMR_TAG,
                     "Socket created, connecting to %s:%d",
                     ip_address_str.c_str(),
                     MODBUS_REGISTERS.ServerPort);

            err = connect(sock,
                          reinterpret_cast<struct sockaddr *>(&dest_addr),
                          sizeof(struct sockaddr_in6));
        }
        else
        {
            auto dest_addr = sockaddr_in6{};
            inet_pton(AF_INET, ip_address_str.c_str(), &dest_addr.sin6_addr);
            dest_addr.sin6_family  = AF_INET6;
            dest_addr.sin6_port    = htons(MODBUS_REGISTERS.ServerPort);
            const auto addr_family = AF_INET6;
            const auto ip_protocol = IPPROTO_IPV6;

            sock = socket(addr_family, SOCK_STREAM, ip_protocol);
            if (sock < 0)
            {
                ESP_LOGE(PMR_TAG, "Unable to create socket: errno %d", errno);
                break;
            }

            auto sock_opt_result =
                setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &socket_timeout, sizeof(socket_timeout));
            if (sock_opt_result < 0)
            {
                ESP_LOGE(PMR_TAG,
                         "Failed to set socket timeout (RECV): %s",
                         std::strerror(sock_opt_result));
                esp_restart();
            }

            sock_opt_result =
                setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &socket_timeout, sizeof(socket_timeout));
            if (sock_opt_result < 0)
            {
                ESP_LOGE(PMR_TAG,
                         "Failed to set socket timeout (SEND): %s",
                         std::strerror(sock_opt_result));
                esp_restart();
            }

            ESP_LOGI(PMR_TAG,
                     "Socket created, connecting to %s:%d",
                     ip_address_str.c_str(),
                     MODBUS_REGISTERS.ServerPort);

            err = connect(sock,
                          reinterpret_cast<struct sockaddr *>(&dest_addr),
                          sizeof(struct sockaddr_in6));
        }

        if (err != 0)
        {
            ESP_LOGE(PMR_TAG, "Socket unable to connect: errno %d", errno);
            esp_restart();
        }
        ESP_LOGI(PMR_TAG, "Successfully connected");

        handle_and_respond_to_modbus_messages(sock);

        if (sock != -1)
        {
            ESP_LOGE(PMR_TAG, "Shutting down socket and restarting...");
            shutdown(sock, 0);
            close(sock);
        }
    }

    vTaskDelete(nullptr);
}

extern "C" void entrypoint()
{
    configure_nvs();
    configure_led();
    configure_outputs();
    configure_rs485();
    configure_wifi();

    xTaskCreate(tcp_client_task, "tcp_client", 4096, nullptr, 5, nullptr);

    for (;;)
    {
        // Main task just blinks LED.
        blink_led();
        vTaskDelay(1000);
    }
}
