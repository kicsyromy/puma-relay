#include "ota.hh"
#include "pmr.hh"

#include <esp_https_ota.h>
#include <esp_log.h>
#include <esp_ota_ops.h>

// FIXME: We get a conversion error caused by fmt::format so disable the warning in this translation
//        unit
HEDLEY_DIAGNOSTIC_PUSH
PMR_DIAGNOSTIC_IGNORE_CONVERSION
#include <fmt/format.h>
HEDLEY_DIAGNOSTIC_POP

#include <array>
#include <charconv>

namespace
{
    struct
    {
        void *handle{ nullptr };
        bool  connection_established{ false };
    } STATE;
} // namespace

OtaError ota_establish_connection(OtaConfig &&config)
{
    esp_http_client_config_t http_config{};
    http_config.user_data         = nullptr;
    http_config.url               = config.url.data();
    http_config.cert_pem          = reinterpret_cast<const char *>(config.ota_server_certificate);
    http_config.keep_alive_enable = true;
    http_config.event_handler     = [](esp_http_client_event_t *event) noexcept {
        switch (event->event_id)
        {
        case HTTP_EVENT_ERROR:
            ESP_LOGE(PMR_TAG, "OTA: Error");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(PMR_TAG, "OTA: Connected");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(PMR_TAG, "OTA: Headers Sent");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(PMR_TAG,
                     "OTA: Headers Received; %s %s",
                     event->header_key,
                     event->header_value);
            break;
        case HTTP_EVENT_ON_DATA: {
            ESP_LOGI(PMR_TAG, "OTA: Data Received");
            break;
        }
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(PMR_TAG, "OTA: Finished");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(PMR_TAG, "OTA: Disconnected");
            break;
        default:
            break;
        }
        return ESP_OK;
    };

    auto ota_config        = esp_https_ota_config_t{};
    ota_config.http_config = &http_config;

    switch (esp_https_ota_begin(&ota_config, &STATE.handle))
    {
    case ESP_OK:
        STATE.connection_established = true;
        return OtaError::Ok;
    case ESP_ERR_INVALID_ARG:
        return OtaError::InvalidArgument;
    }

    return OtaError::Unknown;
}

OtaError ota_close_connection()
{
    auto status = esp_https_ota_abort(STATE.handle);
    if (status == ESP_OK)
    {
        STATE.handle = nullptr;
        return OtaError::Ok;
    }

    return OtaError::Unknown;
}

OtaError ota_is_update_available(bool &result)
{
    result = false;

    if (!STATE.connection_established)
    {
        return OtaError::NoConnection;
    }

    auto image_info = esp_app_desc_t{};
    switch (esp_https_ota_get_img_desc(STATE.handle, &image_info))
    {
    case ESP_OK:
        break;
    case ESP_ERR_INVALID_ARG:
        return OtaError::NoConnection;
    case ESP_FAIL:
        return OtaError::ImageInfoReadFailed;
    default:
        return OtaError::Unknown;
    }

    const auto &current_app_sha256   = esp_app_get_description()->app_elf_sha256;
    auto        current_image_sha256 = std::array<char, 64 + 1>{};
    for (std::size_t i = 0; i < 32; ++i)
    {
        fmt::format_to(current_image_sha256.begin() + i * 2, "{:02x}", current_app_sha256[i]);
    }

    auto update_sha256 = std::array<char, 64 + 1>{};
    for (std::size_t i = 0; i < 32; ++i)
    {
        fmt::format_to(update_sha256.begin() + i * 2, "{:02x}", image_info.app_elf_sha256[i]);
    }

    ESP_LOGI(PMR_TAG,
             "\n***************************************************"
             "\n  Active: %.*s"
             "\n  OTA: %.*s"
             "\n  Update compile time: %.*s"
             "\n  Update compile date: %.*s"
             "\n***************************************************",
             current_image_sha256.size(),
             current_image_sha256.data(),
             update_sha256.size(),
             update_sha256.data(),
             sizeof(image_info.time),
             image_info.time,
             sizeof(image_info.date),
             image_info.date);

    result = std::memcmp(current_app_sha256, image_info.app_elf_sha256, 32) != 0;
    return OtaError::Ok;
}

OtaError ota_execute_update()
{
    auto ota_status = ESP_ERR_HTTPS_OTA_IN_PROGRESS;
    std::puts("");
    while (ota_status == ESP_ERR_HTTPS_OTA_IN_PROGRESS)
    {
        ota_status = esp_https_ota_perform(STATE.handle);
        std::printf(".");
    }
    std::puts("");

    ota_status = ota_status == ESP_OK ? esp_https_ota_finish(STATE.handle) : ota_status;
    if (ota_status == ESP_OK)
    {
        STATE.handle = nullptr;
        return OtaError::Ok;
    }
    else
    {
        switch (ota_status)
        {
        case ESP_ERR_INVALID_ARG:
            return OtaError::InvalidArgument;
        case ESP_ERR_OTA_VALIDATE_FAILED:
            return OtaError::ValidateFailed;
        case ESP_ERR_NO_MEM:
            return OtaError::AllocationFailed;
        }
    }

    return OtaError::Unknown;
}

OtaError ota_remote_compilation_date(PmrTimePoint &result)
{
    if (!STATE.connection_established)
    {
        return OtaError::NoConnection;
    }

    auto image_info = esp_app_desc_t{};
    switch (esp_https_ota_get_img_desc(STATE.handle, &image_info))
    {
    case ESP_OK:
        break;
    case ESP_ERR_INVALID_ARG:
        return OtaError::NoConnection;
    case ESP_FAIL:
        return OtaError::ImageInfoReadFailed;
    default:
        return OtaError::Unknown;
    }

    std::string_view time{ image_info.time };
    std::int32_t     hours{};
    std::int32_t     minutes{};
    std::int32_t     seconds{};

    auto separator_position = time.find_first_of(':');
    for (std::size_t i = 0; i < 3; ++i)
    {
        if (i != 2 && separator_position == std::string_view::npos)
        {
            break;
        }

        auto slice = time.substr(0, separator_position);

        std::uint8_t value{};
        std::from_chars(slice.data(), slice.data() + slice.size(), value);

        switch (i)
        {
        case 0:
            hours = value;
            break;
        case 1:
            minutes = value;
            break;
        case 2:
            seconds = value;
            break;
        }

        time               = time.substr(separator_position + 1);
        separator_position = time.find_first_of(':');
    }

    std::string_view date{ image_info.date };
    std::int32_t     month{};
    std::int32_t     day{};
    std::int32_t     year{};

    separator_position = date.find_first_of(' ');
    for (std::size_t i = 0; i < 3; ++i)
    {
        if (i != 2 && separator_position == std::string_view::npos)
        {
            break;
        }

        auto slice = date.substr(0, separator_position);

        switch (i)
        {
        case 0:
            if (slice == std::string_view{ "Jan" })
            {
                month = 0;
            }
            else if (slice == std::string_view{ "Feb" })
            {
                month = 1;
            }
            else if (slice == std::string_view{ "Mar" })
            {
                month = 2;
            }
            else if (slice == std::string_view{ "Apr" })
            {
                month = 3;
            }
            else if (slice == std::string_view{ "May" })
            {
                month = 4;
            }
            else if (slice == std::string_view{ "Jun" })
            {
                month = 5;
            }
            else if (slice == std::string_view{ "Jul" })
            {
                month = 6;
            }
            else if (slice == std::string_view{ "Aug" })
            {
                month = 7;
            }
            else if (slice == std::string_view{ "Sep" })
            {
                month = 8;
            }
            else if (slice == std::string_view{ "Oct" })
            {
                month = 9;
            }
            else if (slice == std::string_view{ "Nov" })
            {
                month = 10;
            }
            else if (slice == std::string_view{ "Dec" })
            {
                month = 11;
            }

            break;
        case 1:
            std::from_chars(slice.data(), slice.data() + slice.size(), day);
            break;
        case 2:
            std::from_chars(slice.data(), slice.data() + slice.size(), year);
            break;
        }

        date               = date.substr(separator_position + 1);
        separator_position = date.find_first_of(' ');
    }

    auto timeinfo    = std::tm{};
    timeinfo.tm_hour = hours;
    timeinfo.tm_min  = minutes;
    timeinfo.tm_sec  = seconds;
    timeinfo.tm_year = year - 1900;
    timeinfo.tm_mon  = month;
    timeinfo.tm_mday = day;

    result = std::chrono::system_clock::from_time_t(std::mktime(&timeinfo));
    return OtaError::Ok;
}

bool ota_try_execute_update(const char *url, const std::uint8_t *certificate)
{
    ESP_LOGI(PMR_TAG, "Checking for OTA update...");

    if (ota_establish_connection({ url, certificate }) == OtaError::Ok)
    {
        bool       is_update_available = false;
        const auto error               = ota_is_update_available(is_update_available);
        if (!is_update_available)
        {
            if (error != OtaError::Ok)
            {
                ESP_LOGE(PMR_TAG,
                         "Error when checking for OTA update: %d",
                         static_cast<int>(error));
                return false;
            }

            ESP_LOGI(PMR_TAG, "No OTA update available...");
            return true;
        }

        ESP_LOGI(PMR_TAG, "Starting OTA update...");
        if (auto result = ota_execute_update(); result != OtaError::Ok)
        {
            ESP_LOGE(PMR_TAG, "OTA update failed: %d", static_cast<int>(result));
            return false;
        }
        else
        {
            ESP_LOGI(PMR_TAG, "Starting OTA successful, rebooting...");
            return true;
        }
    }

    return false;
}
