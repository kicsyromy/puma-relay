#include "wifi.hh"
#include "pmr.hh"

#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>

#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>

#include <algorithm>
#include <cstring>

/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1

namespace
{
    struct
    {
        /* FreeRTOS event group to signal when we are connected*/
        EventGroupHandle_t event_group{ nullptr };
        int                retry_count{ 0 };
        char               ssid[MAX_SSID_LEN]{};
        char               password[MAX_PASSPHRASE_LEN]{};
    } STATE;
} // namespace

static void event_handler(void *, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (STATE.retry_count < CONFIG_PMR_WIFI_DEFAULT_MAXIMUM_RETRY)
        {
            esp_wifi_connect();
            STATE.retry_count++;
            ESP_LOGI(PMR_TAG, "retry to connect to the AP");
        }
        else
        {
            xEventGroupSetBits(STATE.event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(PMR_TAG, "connect to the AP fail");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = static_cast<ip_event_got_ip_t *>(event_data);
        ESP_LOGI(PMR_TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        STATE.retry_count = 0;
        xEventGroupSetBits(STATE.event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_configure(const char *ssid, const char *password, wifi_auth_mode_t auth_mode)
{
    static constexpr auto MAX_SSID_LENGTH   = static_cast<std::size_t>(MAX_SSID_LEN);
    static constexpr auto MAX_PASSWD_LENGTH = static_cast<std::size_t>(MAX_PASSPHRASE_LEN);

    const auto ssid_len   = std::strlen(ssid);
    const auto passwd_len = std::strlen(password);

    STATE.event_group = xEventGroupCreate();

    wifi_config_t wifi_config{};

    std::memset(wifi_config.sta.ssid, 0, MAX_SSID_LENGTH);
    std::memcpy(wifi_config.sta.ssid, ssid, std::min(MAX_SSID_LENGTH - 1, ssid_len));
    // Save SSID for later
    std::memcpy(STATE.ssid, wifi_config.sta.ssid, MAX_SSID_LENGTH);

    std::memset(wifi_config.sta.password, 0, MAX_PASSWD_LENGTH);
    std::memcpy(wifi_config.sta.password, password, std::min(MAX_PASSWD_LENGTH - 1, passwd_len));
    // Save password for later
    std::memcpy(STATE.password, wifi_config.sta.password, MAX_PASSWD_LENGTH);

    wifi_config.sta.threshold.authmode = auth_mode;

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    PMR_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        nullptr,
                                                        &instance_any_id));
    PMR_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        nullptr,
                                                        &instance_got_ip));

    PMR_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    PMR_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));

    ESP_LOGI(PMR_TAG, "WiFi configuration finished.");
}

void wifi_start()
{
    PMR_ERROR_CHECK(esp_wifi_start());

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed
     * for the maximum number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see
     * above) */
    const auto bits = xEventGroupWaitBits(STATE.event_group,
                                          WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                          pdFALSE,
                                          pdFALSE,
                                          portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which
     * event actually happened. */
    if (bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(PMR_TAG, "connected to AP SSID: %s password: %s", STATE.ssid, STATE.password);
    }
    else if (bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(PMR_TAG,
                 "Failed to connect to SSID: %s, password: %s",
                 STATE.ssid,
                 STATE.password);

        esp_restart();
    }
    else
    {
        ESP_LOGE(PMR_TAG, "UNEXPECTED EVENT");
    }
}
