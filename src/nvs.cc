#include "nvs.hh"

#include <nvs.h>
#include <nvs_flash.h>

namespace
{
    struct
    {
        nvs_handle_t handle{};
    } STATE;
} // namespace

esp_err_t nvs_open_namespace(const char *id)
{
    return nvs_open(id, NVS_READWRITE, &STATE.handle);
}

void nvs_close_namespace()
{
    nvs_close(STATE.handle);
    STATE.handle = 0;
}

void nvs_flush()
{
    nvs_commit(STATE.handle);
}

esp_err_t nvs_blob_size(const char *key, std::size_t &result)
{
    return nvs_get_blob(STATE.handle, key, nullptr, &result);
}

esp_err_t nvs_read_blob(const char *key, void *result, std::size_t size)
{
    return nvs_get_blob(STATE.handle, key, result, &size);
}

esp_err_t nvs_write_blob(const char *key, const void *result, std::size_t size)
{
    return nvs_set_blob(STATE.handle, key, result, size);
}
