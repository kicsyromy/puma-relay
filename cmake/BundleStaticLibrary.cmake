# https://stackoverflow.com/questions/37924383/combining-several-static-libraries-into-one-using-cmake
function (bundle_static_library)
    set (
        one_value_args
        TARGET
        OUTPUT_FILE
    )

    cmake_parse_arguments (
        BSL "" "${one_value_args}" "" ${ARGN}
    )

    set (BUNDLED_TARGET_NAME ${BSL_TARGET}_bundled)

    list (APPEND ALL_STATIC_LIBS ${BSL_TARGET})

    function (recursively_collect_dependencies input_target)
        set (INPUT_LINK_LIBRARIES LINK_LIBRARIES)
        get_target_property (INPUT_TYPE ${input_target} TYPE)
        if (${INPUT_TYPE} STREQUAL "INTERFACE_LIBRARY")
            set (INPUT_LINK_LIBRARIES INTERFACE_LINK_LIBRARIES)
        endif ()

        get_target_property (PUBLIC_DEPENDENCIES ${input_target} ${INPUT_LINK_LIBRARIES})

        foreach (dependency IN LISTS PUBLIC_DEPENDENCIES)
            if (TARGET ${dependency})
                get_target_property (ALIAS ${dependency} ALIASED_TARGET)
                if (TARGET ${ALIAS})
                    set (dependency ${ALIAS})
                endif ()

                get_target_property (TYPE ${dependency} TYPE)
                if (${TYPE} STREQUAL "STATIC_LIBRARY")
                    list(APPEND ALL_STATIC_LIBS ${dependency})
                endif ()

                get_property (LIBRARY_ALREADY_ADDED GLOBAL PROPERTY ${BSL_TARGET}_STATIC_BUNDLE_${dependency})
                if (NOT LIBRARY_ALREADY_ADDED)
                    set_property (GLOBAL PROPERTY ${BSL_TARGET}_STATIC_BUNDLE_${dependency} ON)
                    recursively_collect_dependencies (${dependency})
                endif ()
            endif ()
        endforeach ()

        set (ALL_STATIC_LIBS ${ALL_STATIC_LIBS} PARENT_SCOPE)
    endfunction()

    recursively_collect_dependencies (${BSL_TARGET})

    list (REMOVE_DUPLICATES ALL_STATIC_LIBS)

    if (CMAKE_CXX_COMPILER_ID MATCHES "^([Aa]pple[Cc]lang|[Cc]lang|GNU)$")
        file (WRITE ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar.in "CREATE ${BSL_OUTPUT_FILE}\n")

        foreach (target IN LISTS ALL_STATIC_LIBS)
            file (APPEND ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar.in "ADDLIB $<TARGET_FILE:${target}>\n")
        endforeach ()
        file (APPEND ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar.in "SAVE\n")
        file (APPEND ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar.in "END\n")

        file (
            GENERATE
            OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar
            INPUT ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar.in
        )

        set (AR_TOOL ${CMAKE_AR})
        if (CMAKE_INTERPROCEDURAL_OPTIMIZATION)
            set (AR_TOOL ${CMAKE_CXX_COMPILER_AR})
        endif ()

        add_custom_command (
            COMMAND ${AR_TOOL} -M < ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLED_TARGET_NAME}.ar
            OUTPUT ${BSL_OUTPUT_FILE}
            COMMENT "[${PROJECT_ID}] Bundling ${BSL_TARGET}"
            VERBATIM
        )
    elseif (MSVC)
        find_program(LIB_TOOL lib)

        foreach (target IN LISTS ALL_STATIC_LIBS)
            list (APPEND ALL_STATIC_LIBS_FULL_NAMES $<TARGET_FILE:${target}>)
        endforeach ()

        add_custom_command(
            COMMAND ${LIB_TOOL} /NOLOGO /OUT:${BSL_OUTPUT_FILE} ${ALL_STATIC_LIBS_FULL_NAMES}
            OUTPUT ${BSL_OUTPUT_FILE}
            COMMENT "[${PROJECT_ID}] Bundling ${BSL_TARGET}"
            VERBATIM
        )
    else ()
        message (FATAL_ERROR "[${PROJECT_ID}] ${CMAKE_CXX_COMPILER} is not supported for bundling static libraries")
    endif ()

    add_custom_target (${BSL_TARGET}_bundling_target ALL DEPENDS ${BSL_OUTPUT_FILE})
    add_dependencies (${BSL_TARGET}_bundling_target ${BSL_TARGET})

    add_library (_${BUNDLED_TARGET_NAME} STATIC IMPORTED)
    set_target_properties (
        _${BUNDLED_TARGET_NAME}
        PROPERTIES
            IMPORTED_LOCATION ${BSL_OUTPUT_FILE}
            INTERFACE_INCLUDE_DIRECTORIES $<TARGET_PROPERTY:${BSL_TARGET},INTERFACE_INCLUDE_DIRECTORIES>
    )
    add_dependencies (_${BUNDLED_TARGET_NAME} ${BSL_TARGET}_bundling_target)

endfunction()
