function (generate_cmake_config)
    set (
        one_value_args
        TARGET
        CONFIG_FILE_IN
        VERSION_COMPATIBILITY
    )

    cmake_parse_arguments (
        GCC "" "${one_value_args}" "" ${ARGN}
    )

    include(CMakePackageConfigHelpers)
    write_basic_package_version_file(
        "${GCC_TARGET}-config-version.cmake"
        COMPATIBILITY ${GCC_VERSION_COMPATIBILITY}
    )

    set (DEPENDENCY_LIST "")
    function (collect_dependencies target)
        get_target_property (IDEPS ${target} INTERFACE_LINK_LIBRARIES)
        get_target_property (DEPS  ${target} LINK_LIBRARIES)

        list (APPEND DEPS ${IDEPS})
        list (REMOVE_DUPLICATES DEPS)

        foreach (dependency IN LISTS DEPS)
            if (TARGET ${dependency})
                get_target_property (alias ${dependency} ALIASED_TARGET)
                if (TARGET ${alias})
                    set (dependency ${alias})
                endif ()

                list(APPEND DEPENDENCY_LIST ${dependency})
                collect_dependencies (${dependency})
            endif ()
        endforeach ()
        set (DEPENDENCY_LIST ${DEPENDENCY_LIST} PARENT_SCOPE)
    endfunction()

    collect_dependencies(${GCC_TARGET})
    list(REMOVE_DUPLICATES DEPENDENCY_LIST)

    foreach (dep IN LISTS DEPENDENCY_LIST)
        if (TARGET ${dep})
            get_target_property (${dep}_FIND_PKG_STRING ${dep} CMAKE_CONFIG_FIND_PKG_STRING)
            if (NOT (${${dep}_FIND_PKG_STRING} MATCHES ".+-NOTFOUND"))
                set (CMAKE_CONFIG_FIND_PACKAGES "${CMAKE_CONFIG_FIND_PACKAGES}${${dep}_FIND_PKG_STRING}\n")
            endif ()

            get_target_property (${dep}_FIND_PKG_TARGET ${dep} CMAKE_CONFIG_FIND_PKG_TARGET)
            if (NOT (${${dep}_FIND_PKG_TARGET} MATCHES ".+-NOTFOUND"))
                set (CMAKE_CONFIG_DEP_TARGETS "${CMAKE_CONFIG_DEP_TARGETS}${${dep}_FIND_PKG_TARGET}\n")
            endif ()

            get_target_property (${dep}_CFLAGS ${dep} CMAKE_CONFIG_CFLAGS)
            if (NOT (${${dep}_CFLAGS} MATCHES ".+-NOTFOUND"))
                list (APPEND CMAKE_CONFIG_CFLAGS ${${dep}_CFLAGS})
            endif ()

            get_target_property (${dep}_LIBS ${dep} CMAKE_CONFIG_LIBS)
            if (NOT (${${dep}_LIBS} MATCHES ".+-NOTFOUND"))
                list (APPEND CMAKE_CONFIG_LIBS ${${dep}_LIBS})
            endif ()
        endif ()
    endforeach ()

    string (STRIP "${CMAKE_CONFIG_FIND_PACKAGES}" CMAKE_CONFIG_FIND_PACKAGES)
    string (STRIP "${CMAKE_CONFIG_DEP_TARGETS}" CMAKE_CONFIG_DEP_TARGETS)

    set (CMAKE_CONFIG_LIBDIR ${CMAKE_INSTALL_LIBDIR})
    if (WIN32 AND BUILD_SHARED_LIBS AND (NOT MSVC))
        set (CMAKE_CONFIG_LIBDIR ${CMAKE_INSTALL_BINDIR})
    endif ()
    set (CMAKE_CONFIG_INCLUDEDIR ${CMAKE_INSTALL_INCLUDEDIR})
    set (CMAKE_CONFIG_LIBRARY_NAME ${GCC_TARGET})

    if (CMAKE_CONFIG_CFLAGS)
        string (REPLACE " " ";" CMAKE_CONFIG_CFLAGS " ${CMAKE_CONFIG_CFLAGS}")
        list (REMOVE_DUPLICATES CMAKE_CONFIG_CFLAGS)
        string (REPLACE ";" " " CMAKE_CONFIG_CFLAGS " ${CMAKE_CONFIG_CFLAGS}")
    endif ()

    if (CMAKE_CONFIG_LIBS)
        string (REPLACE " " ";" CMAKE_CONFIG_LIBS " ${CMAKE_CONFIG_LIBS}")
        list (REMOVE_DUPLICATES CMAKE_CONFIG_LIBS)
        string (REPLACE ";" " " CMAKE_CONFIG_LIBS " ${CMAKE_CONFIG_LIBS}")
    endif ()

    set (CMAKE_CONFIG_CXX_STANDARD ${${PROJECT_CMAKE_NAMESPACE}_CXX_STANDARD})
    set (CMAKE_CONFIG_CXX_STANDARD_REQUIRED ON)

    configure_file (${GCC_CONFIG_FILE_IN} ${GCC_TARGET}-config.cmake @ONLY)
    install (
        FILES
            ${CMAKE_CURRENT_BINARY_DIR}/${GCC_TARGET}-config.cmake
            ${CMAKE_CURRENT_BINARY_DIR}/${GCC_TARGET}-config-version.cmake
        DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake
    )
endfunction ()
